\section{Experimental Set-ups and Results} \label{experimentsAndResults}

In this section, we describe the different experiments we performed and discuss their outcome. We investigate the effect of different training variants for perceptron (Section~\ref{performancePerceptron}) and compare two different classifiers -- perceptron and decision trees -- for both tasks (Section~\ref{compPerceptronDTs}). Finally we analyse the effect of different feature templates for both tasks -- POS tagging and gene name extraction (Section~\ref{featureAblation}).


\subsection{Performance of Perceptron} \label{performancePerceptron}
With the first experiment, we analyse the performance of perceptron for the two tasks using the full feature set (see Section~\ref{Feature set} and \ref{New Features added}). We also investigate the impact of randomizing the order of training instances for each iteration during the training process (we call this randomized perceptron). 

To evaluate the performance of perceptron and our feature set for both tasks, we train a model on the training set and evaluate it on the test set using F-measure. We use ten iterations for training \footnote{We performed an experiment in which we analyzed the effect of using one to one hundred iterations for training the perceptron. At ten iterations the F$_1$ score converged.}.

For POS tagging we reached a F$_1$ score of 86.49\% and for gene name extraction we reached an F$_1$ score of 64.22\%. 
Unfortunately our results do not reach the state-of-art which is around an F-Score of 97\% for POS tagging and above an F-score of 80\% for gene name extraction from tokenized text (compare to the results of the BioCreative~I challenge \citep{BioCreativeI:2005}).


The second part of this experiment was aimed at investigating how helpful it is to randomize the order of training instances for each of the iterations in perceptron training. To analyse the effect of randomizing the order of training instances for each of the iterations, we record the performance of one hundred runs of the randomized perceptron for each of the tasks and compare it against the performance of the standard perceptron for the respective task. 

We extract the features and create feature vectors for each of the training instances before we start the training process -- and therefore randomize the order of instances. By doing this, we do not negatively effect multi-word expressions (e.g.~longer gene names) and can apply features related to the previous and next token. During testing we do not randomize the order of tokens. 
Like we did for the first part of this experiment, we again train the different models on the training set using ten iterations and evaluate the performance on the test data with F$_1$ measure. %We discuss the results below. %We give the results in section \ref{resultsPerceptron}.

Figure~\ref{fig:performancePerceptron} shows the results for one hundred runs using the randomized perceptron for both POS tagging (blue box plot) and gene name extraction (red box plot). The figure also shows the performance for POS tagging (blue cross) and gene name extraction (red cross) using the standard perceptron training. 
We observe that for POS tagging randomizing the order of tokens for each of the iterations, results in an increase in F score compared to the standard perceptron training for nearly all of the one hundred runs. The highest scoring run had an F$_1$ of 90.14\%, the lowest an F$_1$ of 84.66\% and the median was at 88.62\%. For gene name extraction, randomizing the order of training instances can also lead to an increase, although it is more likely to lead to a decrease in F score compared to the standard training method. The highest achieved F$_1$ was 67.62\% (higher than the F-score achieved using the standard training), the lowest was 54.93\% and the median was at 64.12\% (slightly lower than the F-score reached with standard training).

\begin{figure}[htbp]
	\centering
	\begin{tabular}{cc}
		
		\begin{tikzpicture}
		\begin{axis}[
		boxplot/draw direction = y,
		xtick = {1,2, 3, 4},
		xticklabels = {POS$_{rand}$, POS, Gene$_{rand}$, Gene},
		height = 8cm,
		width = 7cm,
		ylabel = F$_1$,
		]
		
		\addplot+[blue,
		boxplot prepared={
			draw position = 1,
			box extend = 0.5,
			lower whisker = 84.66,
			lower quartile = 88.1,
			median = 88.615,
			upper quartile = 89.02,
			upper whisker = 90.14,
		},
		]
		coordinates{} ;
		
		\addplot [blue, only marks,mark=x] coordinates {(2,86.49)} node[anchor=south west, color=black] {$86.49$};

		\addplot+[red,
		boxplot prepared={
			draw position = 3,
			box extend = 0.5,
			lower whisker = 54.93,
			lower quartile = 62.58,
			median = 64.12,
			upper quartile = 64.92,
			upper whisker = 67.62,
		},
		]
		coordinates{} ;
		
		\addplot[red, only marks, mark=x] coordinates {(4,64.22)} node[anchor = south, yshift=0.075cm, color=black] {$64.22$};
		
		\end{axis}
		\end{tikzpicture}
		
	\end{tabular}
	\caption{Comparison F$_1$ scores using the standard perceptron training and 100 runs of the randomized perceptron training for both tasks.}
	\label{fig:performancePerceptron}
\end{figure}


\subsection{Comparison of Perceptron and Decision Trees} \label{compPerceptronDTs}
With our second experiment, we compare the perceptron algorithm against a different classifier, namely decision trees, for both tasks. Since our implementation of decision trees requires a large amount of working memory and needs several hours for training, we are only using a small subset of roughly 25,000 tokens of the training data (\textit{25k train}). 

We train both the perceptron model and a decision tree model on this subset of the training data and evaluate the performance (in $F_1$) on the complete test data. Again we use the feature set for the respective task described in Sections~\ref{Feature set} and \ref{New Features added} for both classifiers. We use the standard training (not randomizing the order of training instances) and again ten iterations for training the perceptron. For the decision tree, we use mutual information as decision criteria\footnote{We tested both mutual information and information gain and the results differed only slightly. Therefore we only give the results for mutual information here.} and continue splitting the tree until the leave nodes are homogeneous in regards to the label. The results for the comparison of perceptron and decision trees for POS tagging and gene name extraction can be found in Table \ref{tab:comparisonPerceptronDTs}.

For both tasks, the decision tree classifier's performance is inferior to those of the perceptron classifier. However, this might at least partially be due to the fact that the feature set was optimized using the perceptron algorithm. 


\begin{table}[htbp]
	\centering
	\begin{tabular}{lll}
		\toprule
		Method & POS & Gene \tabularnewline
		\midrule
		Perceptron & 76.37 & 52.35 \tabularnewline
		Decision Tree & 74.34 & 45.50 \tabularnewline
		\bottomrule
	\end{tabular}
	\caption{Comparison of the performance on the test data (in F$_1$) of perceptron and decision trees for the two tasks.}
	\label{tab:comparisonPerceptronDTs}
\end{table}



\subsection{Feature Ablation} \label{featureAblation}

In this section, we describe our last experiment, a feature ablation test, which we performed in order to analyse the impact of the different feature templates on the performance for the two tasks. For the feature ablation test, we train a perceptron model on the training data, again using ten iterations and measuring the performance with F$_1$ on the test data. We compare the performance when the full feature set (as described in Sections~\ref{Feature set} and \ref{New Features added}) is used against the performance when one of the feature templates is excluded. The results of the feature ablation experiment can be found in Table~\ref{tab:featureAblation}.

The table shows that for POS tagging the word form of the token itself is the most important feature whereas for gene name extraction the word form of the previous token is more important.

It is quite interesting that the prefixes feature for POS tagging and the vowel consonant feature for gene extraction hurt the performance on the test set, whereas they did help on the development data (which we used to choose our feature sets). 

When looking at the prefix and suffix feature templates, we observe that the suffixes feature is more helpful for POS tagging (the prefixes feature even hurts the performance) while for gene name extraction, the prefixes feature is more important than the suffixes feature. It is not surprising that the suffixes feature works well for identifying POS tags, since many morphological markers in English are suffixes rather than prefixes, but we did not expect the drop in performance for POS tagging caused by the prefixes feature. 
The features for a token midsentence starting with a capital letter and a token containing upper case letters do not seem to have a great impact on the results for POS tagging, same as the word form of the next token which we expected to be of bigger importance.

\begin{table}[htbp]
	\centering
	\begin{tabular}{lrr} % Tested on test set
		\toprule
		Feature Set & \multicolumn{1}{c}{POS} & \multicolumn{1}{c}{Gene} \tabularnewline
		\midrule
		All features 					& 86.49 	& 64.22 \tabularnewline
		\midrule
		- Word form 					& -16.37 	& -3.59 	\tabularnewline
		- Word form $t_{-1}$ 			& -2.88  	& -8.63 	\tabularnewline
		- Word form $t_{+1}$ 			& -0.51  	& -5.18 	\tabularnewline
		- Suffixes						& -1.21 	& -2.37 	\tabularnewline
		- Prefixes						& +1.60	  	& -4.88 	\tabularnewline
		- Collapsed Shape 				& - 		& -2.69 	\tabularnewline
		- Con-Vow Pattern 				& - 		& +0.42 	\tabularnewline
		- $c_1$ upper case 				& -0.70	 	& - 		\tabularnewline
		- $c_1$ upper case (not $w_1$)	& -0.03  	& - 		\tabularnewline
		- Contains upper case 			& -0.02  	& - 		\tabularnewline
		- Contains hyphen 				& -0.70  	& - 		\tabularnewline
		\bottomrule
	\end{tabular}
	\caption{Results (difference in F$_1$) for the feature ablation tests for both tasks ($t_{-1}$ and $t_{+1}$ denote the previous and next token, $c_1$ the first character of the token and $w_1$ the first token of the sentence).}
	\label{tab:featureAblation}
\end{table}



